import requests
import os


def cdstack_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv("CDSTACK_API_URL", default="https://cdstack-api.example.com/")

    return settings.CDSTACK_API_URL


def cdstack_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("CDSTACK_API_USER", default="user")
        password = os.getenv("CDSTACK_API_PASSWD", default="secret")
        return username, password

    username = settings.CDSTACK_API_USER
    password = settings.CDSTACK_API_PASSWD

    return username, password


def cdstack_head(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.head(
        f"{cdstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def cdstack_get_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.get(
        f"{cdstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def cdstack_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{cdstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def cdstack_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{cdstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def cdstack_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{cdstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def cdstack_delete_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{cdstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def cdstack_get_api_credentials_for_customer(mdat_id: int):
    username, password = cdstack_default_credentials()

    if username != mdat_id:
        response = cdstack_get_data(
            f"v1/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password
